# FOSS Alternatives 

### Speed Test
- <b>Libre Speed</b> - No Flash, No Java, No Websocket, No Bullshit. [Alternative to SpeedTest, Fast]

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; https://librespeed.org/

### Chat & Collab
- <b>Zulip</b> - Chat for distributed teams [Alternative to Slack]

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; https://zulip.com/


